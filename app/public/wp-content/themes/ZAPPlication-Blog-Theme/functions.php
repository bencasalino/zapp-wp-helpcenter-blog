<?php

function university_files() {
  // JS bundle
  wp_enqueue_script('main-university-js', get_theme_file_uri('/js/scripts-bundled.js'), NULL, '1.0', true);
  // Roboto
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
  // Montserrat 
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,900');

  //Font Awesome Icons 
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

  // Bootstrap 
  wp_enqueue_style('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');

  // Load style.css
  wp_enqueue_style('university_main_styles', get_stylesheet_uri());




// wp_enqueue_style('font-awesome-use', '//use.fontawesome.com/releases/v5.10.1/css/all.css');
// wp_enqueue_style('font-awesome-pro', '//pro.fontawesome.com/releases/v5.10.1/css/all.css');
// wp_enqueue_style('fap',  get_stylesheet_uri() . '../css/all.css');
}

add_action('wp_enqueue_scripts', 'university_files');

function university_features() {
  add_theme_support('title-tag');
}

add_action('after_setup_theme', 'university_features');




// move to mu_pugins eventually, so this is not tied to the theme
function helpcenter_post_types() {
    register_post_type('help-center',array(
      'supports'=>array('title', 'editor', 'exceprt'),
      'has_archive'=>true,

        'public'=>true,
        'labels'=> array(
            'name' => 'Help Center',
            'add_new_item' => 'Add New HC Page',
            'edit_item' => 'Edit HC Page',
            'all_items' => 'All HC Pages',
            'sigular_name' => 'Single Page'


        ),
        'menu_icon' => 'dashicons-nametag'
    ));
}


add_action('init', 'helpcenter_post_types');