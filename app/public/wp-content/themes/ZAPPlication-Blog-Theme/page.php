<?php

  get_header();

  while(have_posts()) {
    the_post(); ?>


<div class="header">
                <!-- <h2>Scroll Indicator</h2> -->
                <div class="progress-container">
                  <div class="progress-bar" id="myBar"></div>
                </div> 
              </div>
        <div class="jumbotron-wrapper">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container" id="jumbo-zapp-logo">
                        <div class="heading-primary">
                            <span class="heading-primary--main">HELP CENTER</span>
                            <span class="heading-primary--sub">
                            <?php the_title(); ?>
                            </span>
                            </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
        <div class="row">
            <div class="col-3 sidebar-wrapper">
                <p class="btn-help-center "> <a class="home-help-center" aria-label="Help Center Homepage" href="<?php echo site_url('/') ?>"><i class="fas fa-home"></i> Help Center Home</a>
                </p>
                <div class="container">
                    <!-- <p class="sidebar-article-heading"> Articles in this section</p>
                    <p class="sidebar-article">
                        <a aria-label="Permissible Uses" href="#permissible-uses" class="sidebar-article-link">
                            Creating an Account
                        </a>
                    </p>
                    <p class="sidebar-article">
                        <a aria-label="Prohibited Uses" href="#prohibited-uses" class="sidebar-article-link">
                            Where to find Events
                        </a>
                    </p>
                    <p class="sidebar-article">
                        <a aria-label="Proper Usage Rules" href="#proper-usage-rules" class="sidebar-article-link">
                            Applying to an Event
                        </a>
                    </p>
                    <p class="sidebar-article">
                        <a aria-label="Trademark List" href="#trademark-list" class=" sidebar-article-link">
                            Selecting Images
                        </a>
                    </p>
                    <p class="sidebar-article">
                        <a aria-label="Logo Uses" href="#logo-uses" class="sidebar-article-link">
                            Finalizing Image Order
                        </a>
                    </p>
                    <p class="sidebar-article">
                        <a aria-label="Misuse of Trademark by Others" href="#misuse-of-trademark"
                            class="sidebar-article-link">
                            Submitting an Application
                        </a>
                    </p>
                    <p class="sidebar-article">
                        <a aria-label="Misuse of Trademark by Others" href="#misuse-of-trademark"
                            class="sidebar-article-link">
                            ZAPP® Shop
                        </a>
                    </p>
                    <p class="sidebar-article">
                        <a aria-label="Misuse of Trademark by Others" href="#misuse-of-trademark"
                            class="sidebar-article-link">
                            Submitting Payment
                        </a>
                    </p>
                    <p class="sidebar-article">
                        <a aria-label="Misuse of Trademark by Others" href="#misuse-of-trademark"
                            class="sidebar-article-link">
                            Submitting Multiple Applications
                        </a>
                    </p> -->
                    <hr>
                    <div class="list-group">
                        <br>
                        <p style="text-align: center; color:#A6ADB4;">Help Center Topics </p>
                        <a aria-label="How to Apply" href="<?php echo site_url('/') ?>" 
                            class=" active-page list-group-item list-group-item-action">
                            <i class="fas fa-calendar-check"></i> How to Apply</a>
                        <a aria-label="Uploading Images" href="<?php echo site_url('/uploading-images') ?>" 
                            class="list-group-item list-group-item-action"> <i class="fas fa-images"></i> Uploading
                            Images</a>
                        <a aria-label="How to Pay Booth Fees" href="<?php echo site_url('/how-to-pay-booth-fees') ?>" 
                            class="list-group-item list-group-item-action">
                            <i class="fas fa-money-check-alt"></i> How to Pay Booth Fees </a>
                        <a aria-label="Help Videos" href="<?php echo site_url('/help-videos') ?>" 
                            class="list-group-item list-group-item-action">
                            <i class="fas fa-video"></i> Help Videos </a>
                        <a 
                        aria-label="Using Coupons" href="<?php echo site_url('/using-coupons') ?>" 
                            class="list-group-item list-group-item-action">
                            <i class="fas fa-receipt"></i> Using Coupons</a>
                        <a aria-label="Managing Applications" href="<?php echo site_url('/how-to-manage-applications') ?>" 
                            class="list-group-item list-group-item-action">
                            <i class="fas fa-tasks"></i> Managing Applications</a>
                        <a aria-label="New Features & Fixes" href="<?php echo site_url('/new-features-fixes') ?>" 
                            class="list-group-item list-group-item-action">
                            <i class="fas fa-tools"></i> New Features & Fixes</a>
                        <a aria-label="Give Us Feedback" href="<?php echo site_url('/give-us-feedback') ?>" 
                            class="list-group-item list-group-item-action">
                            <i class="fas fa-comments"></i> Give Us Feedback</a>
                        <a aria-label="Frequently Asked Questions" href="<?php echo site_url('/frequently-asked-questions') ?>" 
                            class="list-group-item list-group-item-action">
                            <i class="fas fa-question-circle"></i> Frequently Asked Questions</a>
                    </div>
                    <br>
                    <div class="list-group">
                        <p style="text-align: center; color:#A6ADB4;">Legal Policies
                        </p>
                        <a aria-label="Terms of Use" href="terms-of-use.html"
                            class="list-group-item list-group-item-action"> <i class="fas fa-file-contract"></i> Terms
                            of
                            Use</a>
                        <a aria-label="Privacy Policy" href="privacy-policy.html"
                            class="list-group-item list-group-item-action"><i class="fas fa-user-lock"></i> Privacy
                            Policy</a>
                        <a aria-label="Refund and
                        Shipping Policy" href="refund-shipping-policy.html"
                            class="list-group-item list-group-item-action"> <i class="fas fa-shipping-fast"></i> Refund
                            and
                            Shipping Policy</a>
                        <a aria-label="Communication Policy" href="communication-policies.html"
                            class="list-group-item list-group-item-action"><i class="fas fa-comments"></i> Communication
                            Policy</a>
                        <a aria-label="Trademark Guidelines" href="trademark-guidelines.html"
                            class=" list-group-item list-group-item-action"><i class="fas fa-gavel"></i> Trademark
                            Guidelines</a>
                        <a aria-label="Software Compatibility" href="software-compatibility.html"
                            class="list-group-item list-group-item-action"> <i class="fas fa-desktop"></i> Software
                            Compatibility</a>
                        <a aria-label="Security" href="zapp-security.html"
                            class="list-group-item list-group-item-action"> <i class="fas fa-shield-alt"></i>
                            Security</a>
                    </div>
                    <br>
                    <p style="text-align: center; color:#444;">Didn't find your answer?
                        <br>
                        Help us improve 
                         <a href="<?php echo site_url('/') ?>"> 
                        <img style="width:60px;" alt="ZAPPlication Logo"
                            src="/wp-content/themes/ZAPPlication-Blog-Theme/images/zapp-logo.png"></a>
                        <br><br>
                        <a href="<?php echo site_url('/give-us-feedback') ?>" aria-label="Give us feedback" style="font-size:18px;"> <i
                            class="fas fa-comments"></i> Give us Feedback</a> <br>
                        <hr>
                    </p>
                    <br>
                </div>
            </div>
            <div class="col main-text">
                <div class="container main-article-padding">
                
                  <?php the_content(); ?>

                <div class="rating">
                          Was this article Helpful?
                </div>
                <div class="article-notes">
                Category:  <?php the_category(', '); ?> | Tags: <?php the_tags(); ?>
                <br>
                Last Updated on <?php the_time('F jS, Y'); ?>
                <br> 
                View all articles by <?php the_author_posts_link(); ?>

                </div>
            <div>

            </div>


                </div>

            </div>
        </div>
    </div>


<script>
// When the user scrolls the page, execute myFunction 
window.onscroll = function() {myFunction()};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}</script>
</body>
<!-- <footer>
</footer> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>



  <?php }

  get_footer();

?>