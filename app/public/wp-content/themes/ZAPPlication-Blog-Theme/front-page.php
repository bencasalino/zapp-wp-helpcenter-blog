<?php get_header();

?>


<!-- <body> -->
    
    <div id="jumbotron-wrapper">
        <div class="jumbotron jumbotron-fluid">
            <div class="container" id="jumbo-zapp-logo">
                <div class="heading-primary">
                    <span class="heading-primary--main">HELP CENTER</span>
                    <span class="heading-primary--sub">Welcome to the ZAPP Help
                        Center!</span>
                    </div>
            </div>
        </div>
    </div>
    <div class="grid-wrapper">
            <div class="card btn">
                    <div class="card-body">
                        <a href="how-to-apply.html" style="text-decoration: none;">
                            <div class="card-title">
                                <i class=" card-main-icon fas fa-calendar-check fa-2x"></i>
                            </div>
                            <h5 class="card-title">
                                How to Apply
                            </h5>
                        </a>
                    </div>
                </div>
                <div class="card btn">
                    <div class="card-body">
                        <a href="uploading-images.html" style="text-decoration: none;">
                            <div class="card-title">
                                <i class=" card-main-icon fas fa-images fa-2x"></i>
                            </div>
                            <h5 class="card-title">
                                Uploading Images
                            </h5>
                        </a>
                    </div>
                </div>
                <div class="card btn">
                    <div class="card-body">
                        <a href="how-to-pay-booth-fees.html" style="text-decoration: none;">
                            <div class="card-title">
                                <i class=" card-main-icon fas fa-money-check-alt fa-2x"></i>
                            </div>
                            <h5 class="card-title">How to Pay Booth Fees</h5>
                            </h5>
                        </a>
                    </div>
                </div>
                    </div>
<div class="grid-wrapper">
        <div class="card btn btn--card">
                <div class="card-body">
                    <a href="help-videos.html" style="text-decoration: none;">
                        <div class="card-title">
                            <i class=" card-main-icon fas fa-video fa-2x"></i>
                        </div>
                        <h5 class="card-title"> Help Videos</h5>
                    </a>
                </div>
            </div>
            <div class="card btn btn--card">
                <div class="card-body">
                    <a href="checkout-with-a-coupon.html" style="text-decoration: none;">
                        <div class="card-title">
                            <i class=" card-main-icon fas fa-receipt fa-2x"></i>
                        </div>
                        <h5 class="card-title"> Checkout with a coupon</h5>
                    </a>
                </div>

            </div>
            <div class="card btn btn--card">
                <div class="card-body">
                    <a href="how-to-manage-applications.html" style="text-decoration: none;">
                        <div class="card-title">
                            <i class=" card-main-icon fas fa-tasks fa-2x"></i>
                        </div>
                        <h5 class="card-title"> How to Manage Applications</h5>
                    </a>
                </div>
            </div>
                </div>

                <div class="grid-wrapper">
                        <div class="card btn btn--card">
                                <div class="card-body">
                                    <a href="new-features-and-fixes.html" style="text-decoration: none;">
                                        <div class="card-title">
                                            <i class=" card-main-icon fas fa-tools fa-2x"></i>
                                        </div>
                                        
                                        <h5 class="card-title"> New Features & Fixes</h5>
                                    </a>
                                </div>
                
                            </div>
                            <div class="card btn btn--card">
                                <div class="card-body">
                                    <a href="give-us-feedback.html" style="text-decoration: none;">
                                        <div class="card-title">
                                            <i class=" card-main-icon fas fa-comments fa-2x"></i>
                                        </div>
                                        <h5 class="card-title">Give Us Feedback</h5>
                                    </a>
                                </div>
                
                            </div>
                            <div class="card btn btn--card">
                                <div class="card-body">
                                    <a href="faq.html" style="text-decoration: none;">
                                        <div class="card-title">
                                            <i class=" card-main-icon fas fa-question-circle fa-2x"></i>
                                        </div>
                                        <h5 class="card-title">Frequently Asked Questions</h5>
                                    </a>
                                </div>
                            </div>
                                </div>
                   
</div>
    </div>
    </div>
    </div>
    <div class="container footer-feedback">
        <hr>
        <p style="text-align: center;"> How can we make the Help Center better? <a class="link" href="#">Give us
                feedback</a> <span>|</span> <a href="#" class="link">Contact Support</a> </p>
    </div>
<!-- </body> -->

<?php get_footer();

?>

