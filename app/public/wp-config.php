<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm3viq3LsGwqfZypXsko/+zv2sE/M0O4rAWgmvLiBXSkPvw/r2MEaevRrgCYA0Y5o+WTboGPk1BWMJJpRKRahRA==');
define('SECURE_AUTH_KEY',  'u4HBr8VkyA8HC6YZhaWTdO0TDuJIkIaWojmdHjFgURqzU66EZcWK5gB07qFpslC3SAZ+i7ikcO9c7GTeOH6kSA==');
define('LOGGED_IN_KEY',    'V+RQQcjaKsTcUV+BSLRokRoNIPD5lMInWPLUXrQuBjVjDIBbCPiKZ0drsUQxLVJErm1eHAhMgJw4VAys2VFc/g==');
define('NONCE_KEY',        'iKt/NPcjo4WLj/UM3+WrLp29RaPJ7GctEwOohtudh2ZS+mf1ryGG0tbWoMzMaFAE+IRqDPW89KkrUdXJL3eHCQ==');
define('AUTH_SALT',        'X6kX816caVpV8HR91+SQmEK4/AfAaUnmzxPo5prrVtg81uV4ZxVyx4I97w/iFbu1o+18EeA/zooVDVjvY1wXAw==');
define('SECURE_AUTH_SALT', '54N+Vx/7WbrXL+kIlzKF9pjp+hwK4ZWYXCi66fTNVdZOxx+m2Cl7BQrDJgsFx0YdIOV5+D5nppcXYAMJWVIRGw==');
define('LOGGED_IN_SALT',   '5OxL55vvX+vJOGwTzMXYrGLgfT9RPbosgMPAGpGb99xdj4PVNP1TreMh0IXbpRGekf6cKHBEriFyCBRmApg8GQ==');
define('NONCE_SALT',       '0MkQdSQ+mYtKV6HkwZjngJWPF5ytOn66XHzgJux2j7LSf1zKP9BT7p6LZSU9MkrtI3aAOM0vGlPCBo14klupJA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
